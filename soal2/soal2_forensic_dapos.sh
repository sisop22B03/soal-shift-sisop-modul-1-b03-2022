#!/bin/bash

# pada soal, bagian struktur repo
# tidak diberi tempat untuk menaruh file "log_website_daffainfo.log",
# jadi diasumsikan ditaruh di direktori yang sama dengan script ini

# function untuk menghitung
# rata-rata serangan per jam
hitung_rata_rata_request_per_jam() {
    awk 'BEGIN {
        # buat bikin direktori
        cmd_mkdir="mkdir -p forensic_log_website_daffainfo_log"
        system(cmd_mkdir)
        close(cmd_mkdir)
    }'

    # NR = number of records
    # adalah jumlah baris yang sudah di-scan
    # oleh awk
    awk '
        BEGIN {
            FS=":"
        }
        waktu != $3 && NR > 1 {
            ++jam
        }
        NR > 1 {
            waktu = $3
        }
        END {
            print "Rata-rata serangan adalah sebanyak", (NR-1)/jam, "requests per jam"
        }' ./log_website_daffainfo.log >> ./forensic_log_website_daffainfo_log/ratarata.txt
}

# function untuk mencari tahu
# IP address yang paling banyak
# mengakses server
ip_attack_frequency() {
    # gsub pada awk digunakan untuk
    # mencari pattern pada string dan menggantinya,
    # di sini digunakan untuk menghilangkan
    # double quotes (")
    awk '
        BEGIN {
            FS=":"
            highest=0
            ip_address=""
        }
        NR == 1 { next }
        {
            ip[$1]++
            if (ip[$1] > highest) {
                highest = ip[$1]
                ip_address = $1
            }
        }
        END {
            gsub(/"/, "", ip_address) # untuk mengganti substring
            print "IP yang paling banyak mengakses server adalah:", ip_address, "sebanyak", highest, "requests\n"
        }' ./log_website_daffainfo.log >> ./forensic_log_website_daffainfo_log/result.txt
}

# function untuk mencari tahu
# request yang menggunakan curl
# sebagai user-agent
count_curl_request() {
    awk '
        BEGIN { FS=":" }
        NR == 1 { next }
        /curl/ { ++n }
        END {
            print "Ada", n, "requests yang menggunakan curl sebagai user-agent\n"
        }' ./log_website_daffainfo.log >> ./forensic_log_website_daffainfo_log/result.txt
}

# function untuk meng-extract
# IP address yang menyerang
# di jam 2 pagi
attacks_2_am() {
    awk '
        BEGIN { FS=":" }
        NR == 1 { next }

        # agar tidak duplicate
        # jika IP address tidak sama dengan baris sebelumnya, print IP tersebut
        ip_address != $1 && $3 == "02" {
            ip_address = $1
            gsub(/"/, "", $1)
            print $1
        }' ./log_website_daffainfo.log >> ./forensic_log_website_daffainfo_log/result.txt
}

# main function
main() {
    # mkdir harus di dalam awk
    # mkdir -p forensic_log_website_daffainfo_log

    hitung_rata_rata_request_per_jam
    ip_attack_frequency
    count_curl_request
    attacks_2_am
}

main