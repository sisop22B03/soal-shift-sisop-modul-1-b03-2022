#!/bin/bash

# function untuk memonitor ram
# dan memasukkan ke dalam log
monitor_ram() {
    # output dari free -m dijadikan input awk
    free -m | awk '
        BEGIN {
            print "mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size"
        }
        {
            if (NR == 1)
                next
            if (max_nf < NF)
                max_nf = NF
            for (i = 2; i <= NF; i++) {
                data[NR, i] = $i
            }
        }
        END {
            for (i = 2; i <= NR; i++) {
                for (j = 2; j <= max_nf; j++) {
                    if (data[i, j] == "")
                        break
                    printf("%s,", data[i, j])
                }
            }
        }' > /home/"$USER"/log/"metrics_$metrics_time_format".log

        # untuk meng-set permission agar hanya
        # user yang membuat file,
        # yang bisa membuka dan memodifikasi file log
        chmod 600 -- /home/"$USER"/log/"metrics_$metrics_time_format".log
}

# function untuk memonitor disk pada /home/$USER/
# dan memasukkan ke log
monitor_disk() {
    du -sh /home/"$USER" | awk '{ print $2"/,"$1 }' >> /home/"$USER"/log/"metrics_$metrics_time_format".log
}

# function untuk membuat cron job untuk
# mengeksekusi script ini setiap menit
make_cron_jobs() {
    # jika sudah ada file crontab bernama "my_cron_jobs",
    # tidak perlu bikin lagi
    if [ ! -f "my_cron_jobs" ]
    then
        # copy cronjob system yang sudah ada dan taruh di file "my_cron_jobs"
        crontab -l > my_cron_jobs

        # echo isi cronjob untuk menjalankan setiap menit dan menjalankan
        # script ini, lokasi script didapatkan dengan absolute path menggunakan `pwd`
        # dan di awk untuk menghilangkan spasi pada nama direktori
        echo "* * * * * "$(pwd | awk '{ gsub(/ /, "\\ ", $0); print; }')"/$0" >> my_cron_jobs

        # masukkan isi dari my_cron_jobs ke crontab system
        crontab my_cron_jobs
    fi
    # pastiin service buat ngejalanin cron udah nyala dan aktif
}

main () {
    # format nama file log
    metrics_time_format=$(date '+%Y%m%d%H%M%S')

    # buat direktori
    awk 'BEGIN {
        cmd_mkdir = "mkdir -p /home/$USER/log"
        system(cmd_mkdir)
        close(cmd_mkdir)
    }'

    monitor_ram
    monitor_disk
    make_cron_jobs
}

main