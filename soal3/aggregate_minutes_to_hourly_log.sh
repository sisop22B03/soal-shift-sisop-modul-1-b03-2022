#!/bin/bash

# Function untuk meng-scan file "log per menit" satu per satu
# dan mendapatkan data yang diminta soal
parse_files() {
    # Di sini, input file ke awk menggunakan wildcard "*" untuk input
    # banyak file sekaligus ke awk, yaitu file yang berawalan
    # %Y%m%d%H*.log

    # NF = number of fields
    # NR = number of records (total baris yg sudah di-scan dari SELURUH input file)
    # FNR = current File Number of Records (banyak baris yang sudah di scan dari file saat ini yg sedang dieksekusi awk)
    awk '
        BEGIN {
            FS=","
            print "type,mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size"
        }
        /home/ {
            ++file_count
            home = $(NF - 1)
            if (max_nf < NF)
                max_nf = NF
            for (i = 1; i <= NF; i++) {
                if (NR == FNR) {
                    max[i] = -999999
                    min[i] = 999999
                    jumlah[i] = 0
                }
                if (i == (NF - 1)) {
                    continue
                }
                jumlah[i] += $i
                if (max[i] < $i) {
                    max[i] = $i
                }
                if (min[i] > $i) {
                    min[i] = $i
                }
            }
        }
        END {
            for (i = 1; i <= 3; i++) {
                if (i == 1) {
                    printf("minimum,")
                } else if (i == 2) {
                    printf("maximum,")
                } else if (i == 3) {
                    printf("average,")
                }
                
                for (j = 1; j <= max_nf; j++) {
                    if (j == (max_nf - 1)) {
                        printf("%s,", home)
                        continue
                    }
                    if (i == 1) {
                        printf("%s", min[j])
                    } else if (i == 2) {
                        printf("%s", max[j])
                    } else if (i == 3) {
                        printf("%s", (jumlah[j] / file_count))
                    }
                    if (j != max_nf)
                        printf(",")
                }
                printf("\n")
            }
        }' /home/"$USER"/log/metrics_$(date '+%Y%m%d%H')*.log > /home/"$USER"/log/"metrics_$log_time_format".log

    # setting permission agar
    # hanya pemilik file yang bisa read dan modify file log
     chmod 600 -- /home/"$USER"/log/"metrics_$log_time_format".log
}

# Function untuk membuat cron jobs
make_cron_jobs() {
    if [ ! -f "agregasi_cron" ]
    then
        # Copy cronjob system yang sudah ada dan taruh di file "agregasi_cron"
        crontab -l > agregasi_cron

        # Dilaksanakan setiap jam pada menit 59
        # Menggunakan 59 */1 * * * agar bisa membandingkan semua file "log per menit"
        # sebanyak 60 kali (menit ke 0 sampai menit ke 59)
        # Lokasi script didapatkan dengan absolute path menggunakan `pwd`
        # dan di awk untuk menghilangkan spasi pada nama direktori
        echo "59 */1 * * * "$(pwd | awk '{ gsub(/ /, "\\ ", $0); print; }')"/$0" >> agregasi_cron

        # masukkan isi dari agregasi_cron ke crontab system
        crontab agregasi_cron
    fi
    # Pastiin service buat ngejalanin cron udah nyala dan aktif
}

# Function main
main() {
    log_time_format=$(date '+%Y%m%d%H')

    parse_files
    make_cron_jobs
}

main