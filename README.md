# Laporan Resmi

Ini adalah laporan resmi Modul 1 untuk mata kuliah Sistem Operasi tahun 2022.

**Kelas**: B<br>
**Kelompok**: B03<br>
**Modul**: 1<br>

## Anggota

- Muhamad Ridho Pratama (5025201186)
- Naily Khairiya (5025201244)
- Beryl (5025201029)

## Soal

Soal dapat dilihat di
[sini](https://docs.google.com/document/d/13lHX4hO09jf07y_JFv0BpwunFL-om9eiNFDlIooSW3o)
(harus dengan email ITS).

## Dokumentasi

Berikut adalah dokumentasi, cara pengerjaan, dan kendala selama pengerjaan.

### Screenshot

Jika gambar tidak muncul, buka *link* Google Drive pada [README](./README.md)

#### **Soal 1**

##### **`register.sh`**:

<img src="https://drive.google.com/uc?id=1ZkuwsHbaOTB-_oX6LOiIBrG4s7jAh3px" alt="main function" width="70%"/>

##### **`main.sh`**:

<img src="https://drive.google.com/uc?id=1UkU_v70CJLD3F7RlWskmtAqJs7MXLHSI" alt="main function" width="70%"/>

#### **Soal 2**

##### **`soal2_forensic_dapos.sh`**

<img src="https://drive.google.com/uc?id=1vHrmgwI_kDS-JRsL3imZCaCmsbvsIS7I" alt="main function" width="70%"/>

#### **Soal 3**

##### **`minute_log.sh`**

<img src="https://drive.google.com/uc?id=1drSZSAvgJ1tcWwUSDDaHb2KWuwrGxIUk" alt="main function" width="70%"/>

##### **`aggregate_minutes_to_hourly_log.sh`**

<img src="https://drive.google.com/uc?id=11J49Wkmd7YE6XJasKw7Q0DBn_4NoGafP" alt="main function" width="70%"/>

### Cara Pengerjaan

#### **Soal 1**

- Untuk `register.sh`:
1. Buat function `main()` agar terlihat lebih rapi. Di dalam function `main()` terdapat beberapa function, yaitu:<br>
    `register_username()` untuk registrasi user<br>
    `register_password()` untuk registrasi password user<br><br>
    
    ```bash
    main() {
        # mkdir harus pake awk
        awk 'BEGIN {
            cmd_mkdir="mkdir -p users"
            system(cmd_mkdir)
            close(cmd_mkdir)
        }'
        touch ./users/user.txt

        cDATE=$(date '+%m/%d/%y %H:%M:%S')

        register_username
        register_password
        echo "$username $password" >> ./users/user.txt
    }
    ```

    Serta, ada beberapa function lain, yaitu:<br>
    `check_if_user_exists()` untuk mengecek apakah sudah ada username yang sama<br>
    `check_password_validity()` untuk mengecek apakah password sesuai dengan kriteria soal

2. Buat direktori `./users` dan file `./users/user.txt` di function `main()`:
    ```bash
    awk 'BEGIN {
        cmd_mkdir="mkdir -p users"
        system(cmd_mkdir)
        close(cmd_mkdir)
    }'
    touch ./users/user.txt
    ```

3. 
    ```bash
    register_username() {
        local flag=1
        while [ $flag -eq 1 ]
        do
            printf "Enter username: "
            read username

            check_if_user_exists
            if [ $user_exists == 1 ]
            then
                printf "REGISTER: ERROR User already exists\n"
                echo "$cDATE REGISTER: ERROR User already exists" >> log.txt
            else
                flag=0
            fi
        done  
    }
    ```
    Pada function `register_username()`, kita buat loop selama user tidak memasukkan username yang benar (username sudah terdaftar), kita tandai loop tersebut dengan `local flag=1`<br><br>
    Di dalam loop ini, kita meminta user untuk memasukkan username, dan di-*assign* ke variabel `username`:<br>
    ```bash
    printf "Enter username: "
    read username
    ```
    Setelah user memasukkan username, akan masuk ke function `check_if_user_exists()`

4. 
    ```bash
    check_if_user_exists() {
        user_exists=$(awk -v username="$username" '
            BEGIN {
                exists = 0
            }
            {
                if (username == $1) {
                    exists = 1
                }
            }
            END {
                if (exists == 0)
                    printf(0)
                else
                    printf(1)
            }' ./users/user.txt)
    }
    ```
    Pada function `check_if_user_exists()`, kita akan mengecek apakah username yang dimasukkan user tadi sudah ada di file `./users/user.txt` atau belum, dengan `awk`.

    Kita mengirim variabel `username` dari shell ke `awk`, dengan option `-v`.<br>
    Setelah kami *googling*, di shell bash tidak ada cara untuk mendapatkan return value. Jadi, output `print` dari `awk` kita masukkan ke variabel global `user_exists` agar bisa digunakan di function lain.<br>
    Variabel `user_exists` akan berisi nilai `0` atau `1`, tergantung output dari `awk`, apakah username sudah terdaftar di `./users/user.txt` atau belum.

    Setelah itu, kembali ke function `register_username()`.<br>
    ```bash
    if [ $user_exists == 1 ]
    then
        printf "REGISTER: ERROR User already exists\n"
        echo "$cDATE REGISTER: ERROR User already exists" >> log.txt
    else
        flag=0
    fi
    ```
    Di sini, akan dicek jika variabel `user_exists` bernilai `1` yang berarti user sudah terdaftar, maka akan menampilkan `REGISTER: ERROR User already exists`, dan menambahkan pesan eror tersebut ke dalam file `./log.txt`.<br>
    Jika `user_exists` bernilai `0` yang berarti username belum terdaftar, maka set `flag` menjadi `0` dan keluar loop. Lalu, kembali ke function `main()`.

5. Function `register_password()`
    ```bash 
    register_password() {
        local flag=1
        while [ $flag -eq 1 ]
        do
            printf "Enter password: "
            read -rs password

            check_password_validity
            if [ $password_valid -eq 0 ]
            then
                printf "Password tidak memenuhi kriteria!\n"
            else
                local flag=0
                echo "REGISTER: INFO User $username registered successfully"
                echo "$cDATE REGISTER: INFO User $username registered successfully" >> log.txt
            fi
        done
    }
    ```

    Pada function ini, user akan menginputkan password dan di-*assign* ke variabel `password`.<br>
    Opsi `-r` agar *backslash* tidak meng-*escape* karakter, opsi `-s` agar password yang diketik tampil *hidden*.

    ```bash
    printf "Enter password: "
    read -rs password
    ```
    
    Sama dengan function `register_username()`, kita akan melakukan loop sampai password yang dimasukkan oleh user memenuhi kriteria soal. Kriterianya, yaitu:<br>
    - Minimal 8 karakter
    - Memiliki minimal 1 huruf kapital dan 1 huruf kecil
    - Alphanumeric
    - Tidak boleh sama dengan username

    Pengecekan ini dilakukan di function `check_password_validity()`.

6. Function `check_password_validty()`
    ```bash
    check_password_validity() {
        password_valid=1
        if [ ${#password} -lt 8 ] ||
            [[ ! "$password" =~ [[:upper:]] ]] ||
            [[ ! "$password" =~ [[:lower:]] ]] ||
            [[ ! "$password" =~ ^[[:alnum:]]+$ ]] ||
            [ ${password,,} == ${username,,} ]
        then
            password_valid=0
        fi
    }
    ```
    Pertama, kita buat variabel `password_valid`, agar bisa digunakan di function `register_password()` nanti. Kita *assign* nilai `1`.
    
    Lalu, sesuai kriteria soal, pertama kita cek apakah password kurang dari 8 karakter, lalu apakah password tidak mengandung huruf kapital, lalu apakah password tidak mengandung huruf kecil, lalu apakah password mengandung karakter yang tidak alphanumeric, dan terakhir apakah password sama dengan username.

    Kita menggunakan `=~` untuk membandingkan variabel `password` dengan regular expression (regex). Bash menyediakan beberapa regex, seperti `:upper:`, `:lower:`, dan `:alnum:`.

    Untuk pengecekan apakah username sama dengan password, kita menggunakan string manipulation untuk bash.
    `${string,,}` digunakan untuk menjadikan variabel `string` ke lowercase. Jadi, username dan password bisa dibandingkan apakah sama atau tidak, tidak peduli huruf kapital atau huruf kecil.

    Jika salah satu syarat tadi terpenuhi, maka password tidak memenuhi kriteria. Sebaliknya, jika password melewati semua `if` tersebut, maka password memenuhi kriteria, dan `password_valid` di-set ke `0`.

    Setelah dilakukan pengecekan, kembali ke function `register_password()`.

7. Nilai variabel `password_valid` tadi digunakan untuk menentukan apakah password valid atau tidak.

    ```bash
    if [ $password_valid -eq 0 ]
    then
        printf "Password tidak memenuhi kriteria!\n"
    else
        local flag=0
        echo "REGISTER: INFO User $username registered successfully"
        echo "$cDATE REGISTER: INFO User $username registered successfully" >> log.txt
    fi
    ```
    Jika password valid, maka loop akan berhenti, dan menampilkan log dan menambahkan pesan log ke file `./log.txt`. Sedangkan jika tidak valid, maka loop akan dilakukan lagi, dan user akan diminta untuk memasukkan password lagi.

8. Setelah user terdaftar, maka data username dan password dimasukkan ke file `./users/user.txt` dengan format `username[spasi]password`:
    ```bash
    echo "$username $password" >> ./users/user.txt
    ```

- Untuk `main.sh`:
1. Membuat function main(),
    ```bash
    main() {
        cDATE=$(date '+%m/%d/%y %H:%M:%S')
        user_login
        main_program
    }
    ```
    Berisi:
    - Variabel `cDATE` untuk format pesan log
    - Jalankan function `user_login()` untuk interface login user
    - Jalankan function `main_program()` di mana user bisa menginput command `dl N` dan `att`

    Juga dibuat function-function berikut:
    - `check_user_password()` untuk mengecek password yang diinput user
    - `download_gambar()` untuk memproses command `dl N`
    - `check_login_attempts()` untuk memproses command `att`

2. Pada function `user_login()`:
    ```bash
    user_login() {
        logged_in=0

        printf "Enter username: "
        read username

        while [ $logged_in -eq 0 ]
        do
            printf "Enter password: "
            read -rs password

            check_user_password
            if [ $password_check -eq 0 ]
            then
                echo "Password salah!"
                echo "$cDATE LOGIN: ERROR Failed login attempt on user $username" >> log.txt
            else
                logged_in=1
                echo "LOGIN: INFO User $username logged in"
                echo "$cDATE LOGIN: INFO User $username logged in" >> log.txt
            fi
        done
    }
    ```
    Di sini, kami mengasumsikan username yang diinput user pasti benar dan sudah terdaftar di file `./users/user.txt`

    Dibuat variabel `logged_in` untuk tanda apakah user sudah berhasil login atau belum. Juga untuk loop saat user salah memasukkan password.

    Meminta user untuk memasukkan password, dan di-*assign* ke variabel `password`:
    ```bash
    printf "Enter password: "
    read -rs password
    ```

    Setelah itu, masuk ke function `check_user_password()`

3. Function `check_user_password()`:
    ```bash
    check_user_password() {
        password_check=$(awk -v username="$username" -v password="$password" '
                    {
                        if (username == $1) {
                            if (password != $2)
                                printf(0)
                            else
                                printf(1)
                            exit
                        }
                    }' ./users/user.txt)
    }
    ```
    - Di sini, kita masukkan variabel `username` dan variabel `password` dari shell ke `awk` dengan opsi `-v`.
    
        Lalu, scan file `./users/user.txt` dengan `awk` untuk mengecek apakah password cocok dengan username yang diinput user. Setelah itu, output `print` dari awk (bernilai `0` atau `1`) di-*assign* ke variabel shell `password_check`

        Setelah itu, kembali ke function `user_login()`

    - Mengecek apakah `password_check` bernilai `0` atau `1` untuk menentukan password benar atau tidak.
    ```bash
    if [ $password_check -eq 0 ]
    then
        echo "Password salah!"
        echo "$cDATE LOGIN: ERROR Failed login attempt on user $username" >> log.txt
    else
        logged_in=1
        echo "LOGIN: INFO User $username logged in"
        echo "$cDATE LOGIN: INFO User $username logged in" >> log.txt
    fi
    ```
    - Jika password salah, maka akan menampilkan pesan error dan menambahkan log ke file `./log.txt`

        Sedangkan jika password benar, maka akan menampilkan user berhasil login, dan menambahkan log ke file `./log.txt`, serta keluar dari loop, dan masuk ke function `main_program()`

4. Function `main_program()`:
    ```bash
    main_program() {
        while [ $logged_in -eq 1 ]
        do
            echo "Masukkan perintah/command:"
            read perintah

            if [[ "$perintah" =~ ^dl ]]
            then
                download_gambar
            elif [[ "$perintah" == "att" ]]
            then
                check_login_attempts
            else
                printf "Perintah tidak ada!\n\
                    Perintah yang tersedia:\n\
                    1. dl\n\
                    2. att\n"
            fi
        done
    }
    ```
    Di function ini, user dapat memasukkan command `dl N` atau `att`, di-*loop* sampai script ter-*halt* dengan CTRL-C

    Command yang dimasukkan user di-*assign* ke variabel `perintah`

    Untuk mendeteksi command `dl`, digunakan `=~` untuk membandingkan variabel `perintah` dengan regex yang berawalan dengan `dl`

    Jika user memasukkan command `dl N`, maka akan masuk ke function `download_gambar()`, sedangkan jika user memasukkan command `att`, maka akan masuk ke function `check_login_attempts()`

5. Function `download_gambar()`:
    ```bash
    download_gambar() {
        # ekstrak substring dari command "dl N"
        # format: ${string:position:length}
        local jumlah_gambar=${perintah:3}
        local file_name="$(date '+%Y-%m-%d')_$username"
        awk -v file_name=$file_name 'BEGIN {
            cmd_mkdir="mkdir -p " file_name
            system(cmd_mkdir)
            close(cmd_mkdir)
        }'
        local latest_number=0

        # Cek apakah udah ada file .zip dengan nama yang sama
        if [ -f "$file_name.zip" ]
        then
            # -n agar tidak overwrite file yang sudah ada
            unzip -n -P "$password" "$file_name.zip"

            # Cek nomor terakhir file PIC_XX
            # Argumen -t untuk mengurutkan output ls berdasarkan waktu file terbaru
            latest_number=$(ls -t ./"$file_name" | awk '
                BEGIN {
                    FS="_"
                }
                {
                    latest_number = $2
                    print $2
                    exit
                }')

            # Mengonversi
            # juga mengubah ke base-10, jika ada leading 0,
            # seperti PIC_0X
            latest_number=$((10#$latest_number))
        fi

        # mengunduh dan memberi nama file gambar
        for ((i=latest_number+1; i<=latest_number+jumlah_gambar; i=i+1))
        do
            if [ $i -lt 10 ]
            then
                # -L untuk menunggu redirect
                curl -L https://loremflickr.com/320/240 -o "./$file_name/PIC_0$i"
            else
                curl -L https://loremflickr.com/320/240 -o "./$file_name/PIC_$i"
            fi
        done

        # -r = recursive, biar bisa ngezip dari
        # dalem folder
        zip -r -u -P "$password" -r "$file_name.zip" "$file_name"
    }
    ```
    Pertama, ekstrak substring untuk mendapatkan berapa banyak gambar yang ingin didownload user. Masukkan ke variabel `jumlah_gambar`

    Lalu, buat direktori sesuai perintah soal dengan `awk`.

    Siapkan variabel `latest_number` untuk menyimpan nomor file terakhir, jika sudah ada file `.zip` sebelumnya. Default ke nilai `0` yang berarti belum ada gambar atau file `.zip` sebelumnya

    Cek jika sudah ada file `.zip`:
    ```bash
    if [ -f "$file_name.zip" ]
    then
        unzip -n -P "$password" "$file_name.zip"

        latest_number=$(ls -t ./"$file_name" | awk '
            BEGIN {
                FS="_"
            }
            {
                latest_number = $2
                print $2
                exit
            }')

        latest_number=$((10#$latest_number))
    fi
    ```
    Jika sudah ada file `.zip`, `unzip` file tersebut.

    Dapatkan nomor terakhir file dengan menggunakan command `ls` dengan opsi `-t` untuk mengurutkan file berdasarkan waktu terbaru, maka file dengan nomor terbesar akan berada di baris pertama, ini memudahkan pengambilan dengan `awk`. Simpan output dari `awk` ke variabel `latest_number`.

    Lalu, loop dari `latest_number + 1` sampai `latest_number + jumlah_gambar`. Setiap perulangan, unduh gambar dengan menggunakan `curl` dan opsi `-L` untuk menunggu redirect dari website. Tambahkan juga opsi `-o` untuk menyimpan hasil unduhan dengan format nama file seperti soal (`PIC_XX`).

    `zip` file dengan password yang sama dengan password user, menggunakan opsi `-P`. Beri nama file `.zip` tersebut dengan format nama file sesuai soal.

6. Function `check_login_attempts()`:
    ```bash
    check_login_attempts() {
    awk -v username="$username" '
        BEGIN {
            gagal = 0
            berhasil = 0
        }
        {
            if ($3 == "LOGIN:") {
                if ($NF == username)
                    ++gagal
                else if ($6 == username)
                    ++berhasil
            }
        }
        END {
            print "Percobaan login berhasil:", berhasil
            print "Percobaan login gagal:", gagal
            print "Total percobaan login:", berhasil+gagal
        }' ./log.txt
    }
    ```
    Kita akan mengecek berapa percobaan login dari user yang berhasil, gagal, dan jumlah keduanya. Pengecekan dilakukan dengan `awk` dengan input file `./users/user.txt`

    Kita masukkan variabel `username` dari shell ke `awk` dengan opsi `-v`. Lalu, pada bagian `BEGIN`, set variabel `berhasil` dan `gagal` ke nilai `0`.

    Pada blok utama, cek jika kolom ketiga sama dengan `"LOGIN:"`, jika sama, cek jika `NF` (Number of Fields atau banyak kolom, yang berarti kolom terakhir) sama dengan `username`, jika sama, incremenet variabel `gagal`, jika berbeda, increment variabel `berhasil`.

    Di bagian `END`, tampilkan tiap percobaan login yang berhasil, gagal, dan jumlah keduanya.

#### **Soal 2**

1. Membuat function `main()`:
    ```bash
    main() {
        hitung_rata_rata_request_per_jam
        ip_attack_frequency
        count_curl_request
        attacks_2_am
    }
    ```
    Di dalam function ini, terdapat pengeksekusian beberapa function, yaitu:
    - `hitung_rata_rata_request_per_jam()` untuk menghitung rata-rata request per jam
    - `ip_attack_frequency()` untuk mencari IP address yang paling banyak menyerang dan jumlah serangannya
    - `count_curl_request()` untuk menghitung request yang menggunakan user agent `curl`
    - `attacks_2_am` untuk mencari IP address yang menyerang pukul 2 pagi.

2. Function `hitung_rata_rata_request_per_jam()`:
    ```bash
    hitung_rata_rata_request_per_jam() {
        awk 'BEGIN {
            # buat bikin direktori
            cmd_mkdir="mkdir -p forensic_log_website_daffainfo_log"
            system(cmd_mkdir)
            close(cmd_mkdir)
        }'

        # NR = number of records
        # adalah jumlah baris yang sudah di-scan
        # oleh awk
        awk '
            BEGIN {
                FS=":"
            }
            waktu != $3 && NR > 1 {
                ++jam
            }
            NR > 1 {
                waktu = $3
            }
            END {
                print "Rata-rata serangan adalah sebanyak", (NR-1)/jam, "requests per jam"
            }' ./log_website_daffainfo.log >> ./forensic_log_website_daffainfo_log/ratarata.txt
    }
    ```
    Di function ini, kita mengecek file `log_website_daffainfo.log` dengan `awk` untuk mengetahui berapa rata-rata request per jam.

    Buat direktori `forensic_log_website_daffainfo_log` terlebih dahulu menggunakan `awk`.

    Pertama, kita set variabel `waktu` ke kolom ketiga. Lalu, jika variabel `waktu` baris saat ini tidak sama dengan variabel `waktu` baris sebelumnya (yang berarti jam sudah berganti), maka increment variabel `jam`.

    Di bagian `END`, hitung rata-rata dengan cara banyaknya baris dikurang baris pertama, lalu dibagi variabel `jam`.

    Terakhir, tambahkan hasil output ke file `./forensic_log_website_daffainfo_log/ratarata.txt`.

3. Function `ip_attack_frequency()`
    ```bash
    ip_attack_frequency() {
        # gsub pada awk digunakan untuk
        # mencari pattern pada string dan menggantinya,
        # di sini digunakan untuk menghilangkan
        # double quotes (")
        awk '
            BEGIN {
                FS=":"
                highest=0
                ip_address=""
            }
            NR == 1 { next }
            {
                ip[$1]++
                if (ip[$1] > highest) {
                    highest = ip[$1]
                    ip_address = $1
                }
            }
            END {
                gsub(/"/, "", ip_address) # untuk mengganti substring
                print "IP yang paling banyak mengakses server adalah:", ip_address, "sebanyak", highest, "requests\n"
            }' ./log_website_daffainfo.log >> ./forensic_log_website_daffainfo_log/result.txt
    }
    ```
    Function ini digunakan untuk mencari IP address yang paling banyak menyerang, dan jumlah serangannya.

    Kita menggunakan `awk` untuk meng-*scan* file `log_website_daffainfo.log`.

    Pertama, pada bagian `BEGIN` set terlebih dahulu field separator ke titik dua (:), variabel `highest` ke `0`, dan variabel `ip_address` ke `""`. Lalu lewati baris pertama (`NR == 1 { next }`).

    Setiap kemunculan IP address, increment array `ip[]`, dengan IP address sebagai indeksnya. Jika jumlah kemunculan suatu IP address lebih tinggi dari variabel `highest`, ganti variabel `highest` dengan jumlah kemunculan IP address tersebut dan ganti variabel `ip_address` ke IP address saat ini.

    Pada bagian `END`, hilangkan double quotes pada IP address dengan function `gsub()`, dan print sesuai dengan format soal.

    Terakhir, tambahkan hasil output ke file `./forensic_log_website_daffainfo_log/ratarata.txt`.

4. Function `count_curl_request()`:
    ```bash
    count_curl_request() {
        awk '
            BEGIN { FS=":" }
            NR == 1 { next }
            /curl/ { ++n }
            END {
                print "Ada", n, "requests yang menggunakan curl sebagai user-agent\n"
            }' ./log_website_daffainfo.log >> ./forensic_log_website_daffainfo_log/result.txt
    }
    ```
    Di function ini, kita menghitung jumlah request yang menggunakan `curl` sebagai user-agent. Scan file `log_website_daffainfo.log` dengan `awk`. Ganti field separator dengan titik dua (:). Lewati baris pertama.

    Jika pattern `/curl/` ditemukan, maka increment variabel `n`. Di sini, variabel `n` digunakan untuk menghitung kemunculan kata `curl`.

    Pada bagian `END`, print output sesuai dengan permintaan soal.

    Terakhir, tambahkan hasil output ke file `./forensic_log_website_daffainfo_log/ratarata.txt`.

5. Function `attacks_2_am()`:
    ```bash
    attacks_2_am() {
        awk '
            BEGIN { FS=":" }
            NR == 1 { next }

            # agar tidak duplicate
            # jika IP address tidak sama dengan baris sebelumnya, print IP tersebut
            ip_address != $1 && $3 == "02" {
                ip_address = $1
                gsub(/"/, "", $1)
                print $1
            }' ./log_website_daffainfo.log >> ./forensic_log_website_daffainfo_log/result.txt
    }
    ```
    Function ini digunakan untuk mendapatkan IP address yang menyerang pukul 2 pagi. Scan file `log_website_daffainfo.log` dengan `awk`. Ganti field separator dengan titik dua (:). Lewati baris pertama.

    Pertama, kita set variabel `ip_address` ke kolom pertama, agar bisa dibandingkan dengan baris sebelumnya untuk keperluan baris-baris selanjutnya supaya tidak muncul duplikat. Lalu hilangkan kutip dua (") pada kolom pertama dengan `gsub()`. Setelah itu, print kolom pertama.

    Terakhir, tambahkan hasil output ke file `./forensic_log_website_daffainfo_log/ratarata.txt`.

#### **Soal 3**

- Untuk file `minute_log.sh`
1. Membuat function `main()` supaya terlihat rapi. Pada function `main()`, 
    ```bash
    main () {
        # format nama file log
        metrics_time_format=$(date '+%Y%m%d%H%M%S')

        # buat direktori
        awk 'BEGIN {
            cmd_mkdir = "mkdir -p /home/$USER/log"
            system(cmd_mkdir)
            close(cmd_mkdir)
        }'

        monitor_ram
        monitor_disk
        make_cron_jobs
    }
    ``` 
    - Membuat variabel `metrics_time_format` yang digunakan untuk menyimpan format waktu untuk penamaan file log. 
    - Membuat  direktori  `/home/{user}/log` pada function `main()` untuk menyimpan semua file log pada direktori tersebut

    ```bash
        # buat direktori
        awk 'BEGIN {
            cmd_mkdir = "mkdir -p /home/$USER/log"
            system(cmd_mkdir)
            close(cmd_mkdir)
        }'
    ```
    - Pemanggilan function `monitor_ram()`, `monitor_disk()`, dan juga `make_cron_jobs()`


2. Membuat function `monitor_ram()`, fungsi ini digunakan untuk memonitor ram dan memasukkan semua metrics ke dalam file log dengan format `metrics_{YmdHms}.log`
    ```bash
    # function untuk memonitor ram
    # dan memasukkan ke dalam log
    monitor_ram() {
        # output dari free -m dijadikan input awk
        free -m | awk '
            BEGIN {
                print "mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size"
            }
            {
                if (NR == 1)
                    next
                if (max_nf < NF)
                    max_nf = NF
                for (i = 2; i <= NF; i++) {
                    data[NR, i] = $i
                }
            }
            END {
                for (i = 2; i <= NR; i++) {
                    for (j = 2; j <= max_nf; j++) {
                        if (data[i, j] == "")
                            break
                        printf("%s,", data[i, j])
                    }
                }
            }' > /home/"$USER"/log/"metrics_$metrics_time_format".log

        # untuk meng-set permission agar hanya
        # user yang membuat file,
        # yang bisa membuka dan memodifikasi file log
        chmod 600 -- /home/"$USER"/log/"metrics_$metrics_time_format".log
    }
    ```
    - Membuat pipe untuk memerintahkan output dari `free -m` untuk menjadi input awk. 
    - Didalam awk, print terlebih dahulu isi awal dari file metrics yaitu _“mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,s wap_free,path,path_size”_ 
    - Mengambil nilai-nilai yang dibutuhkan dari output `free -m` dan menyimpan nilai tersebut pada array `data[]`
    - Print nilai pada array `data[]` dengan delimiter `‘,’`
    - Dan memasukkan output awk tersebut ke `/home/"$USER"/log/"metrics_$metrics_time_format".log `
    - Karena file log bersifat sensitif pastikan semua file log hanya dapat dibaca oleh user pemilik file. Oleh karena itu, atur permission agar hanya user yang membuat file, yang bisa membuka dan memodifikasi file log dengan:
    ```bash
    chmod 600 -- /home/"$USER"/log/"metrics_$metrics_time_format".log`
    ```

3. Membuat function `monitor_disk()` yang digunakan untuk memonitor disk pada `/home/$USER/`
    ```bash
    monitor_disk() {
        du -sh /home/"$USER" | awk '{ print $2"/,"$1 }' >> /home/"$USER"/log/"metrics_$metrics_time_format".log
    }
    ```
    
    - Dengan menggunakan pipe maka akan memerintahkan output `du -sh`(untuk monitoring disk pada` /home/$USER/`) untuk menjadi input bagi awk
    - Awk digunakan untuk mendapatkan argument kedua yaitu pathnya dan juga argument pertama untuk path size nya


4. Membuat function `make_cron_jobs()` agar script bisa dieksekusi setiap menit
    ```bash
    # function untuk membuat cron job untuk
    # mengeksekusi script ini setiap menit
    make_cron_jobs() {
        # jika sudah ada file crontab bernama "my_cron_jobs",
        # tidak perlu bikin lagi
        if [ ! -f "my_cron_jobs" ]
        then
            # copy cronjob system yang sudah ada dan taruh di file "my_cron_jobs"
            crontab -l > my_cron_jobs

            # echo isi cronjob untuk menjalankan setiap menit dan menjalankan
            # script ini, lokasi script didapatkan dengan absolute path menggunakan `pwd`
            # dan di awk untuk menghilangkan spasi pada nama direktori
            echo "* * * * * "$(pwd | awk '{ gsub(/ /, "\\ ", $0); print; }')"/$0" >> my_cron_jobs

            # masukkan isi dari my_cron_jobs ke crontab system
            crontab my_cron_jobs
        fi
        # pastiin service buat ngejalanin cron udah nyala dan aktif
    }
    ```

    - Sebelumnya dicek terlebih dahulu apakah sudah ada file crontab Bernama `“my_cron_jobs”`, jika sudah ada tidak perlu dibuat lagi
    - Copy cronjob yang sudah ada pada file `“my_cron_jobs"`
    - Echo isi cronjob untuk menjalankan setiap menit ( * * * * *) dan menjalankan script ini
    - Lokasi script didapatkan dengan absolute path dengan menggunakan `pwd` 
    - Awk digunakan untuk menghilangkan spasi pada nama direktori
    - Masukkan isi dari `my_cron_jobs` ke crontab system


- File `aggregate_minutes_to_hourly_log.sh` untuk membuat agregasi file log ke satuan jam:
1. Membuat function `main()`, 
    ```bash
    # Function main
    main() {
        log_time_format=$(date '+%Y%m%d%H')

        parse_files
        make_cron_jobs
    }
    ```
    - Terdapat `log_time_format` untuk penamaan file log yang mana format waktu hanya sampai jam saja
    - Pemanggilan function `parse_files()` dan `make_cron_jobs`

2. Membuat function `parse_files()` untuk scan file log per menit satu per satu dan mendapatkan data seperti minimum, maxsimum, dan average
    - Input file ke awk menggunakan wildcard `*` untuk input banyak file sekaligus ke awk, yaitu file yang berawalan `%Y%m%d%H` dan berakhiran dengan `.log`

        ```bash
        /home/"$USER"/log/metrics_$(date '+%Y%m%d%H')*.log
        ```
    - Gunakan FS (Field Separator) koma untuk membagi kolom setiap barisnya
    - Print `type,mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_us ed,swap_free,path,path_size` seperti yang diminta pada soal
    - Hitung jumlah file dengan `++file_count` yang akan digunakan sebagai pembagi dalam mencari average
    - Untuk inisialisasi diatur nilai max sekecil-kecilnya, nilai min sebesar-besarnya, serta jumlah = 0

        ```bash
        for (i = 1; i <= NF; i++) {
            if (NR == FNR) {
                max[i] = -999999
                min[i] = 999999
                jumlah[i] = 0
        }
        ```

    - Jumlah akan ditambahkan dengan nilai setiap `$i`

        ```bash
        jumlah[i] += $i
        ```

    - Untuk nilai max dan min juga akan dicek. Apabila nilai max kurang dari nilai sekarang maka nilai max akan diubah menjadi nilai sekarang. Begitu pula dengan nilai min, apabila lebih besar dari nilai sekarang maka nilai min akan diubah menjadi nilai yang sekarang

        ```bash
        if (max[i] < $i) {
            max[i] = $i
        }
        if (min[i] > $i) {
            min[i] = $i
        }
        ```

    - Setelah mendapatkan yang diminta maka print sesuai yang diminta pada soal. Untuk nilai min dan juga max dapat langsung diprint namun untuk nilai average perlu adanya perhitungan dengan ` jumlah[j] / file_count`

        ```bash
        for (i = 1; i <= 3; i++) {
            if (i == 1) {
                printf("minimum,")
            } else if (i == 2) {
                printf("maximum,")
            } else if (i == 3) {
                printf("average,")
            }
            
            for (j = 1; j <= max_nf; j++) {
                if (j == (max_nf - 1)) {
                    printf("%s,", home)
                    continue
                }
                if (i == 1) {
                    printf("%s", min[j])
                } else if (i == 2) {
                    printf("%s", max[j])
                } else if (i == 3) {
                    printf("%s", (jumlah[j] / file_count))
                }
                if (j != max_nf)
                    printf(",")
            }
            printf("\n")
        }
        ```
    - Kemudian output akan disimpan pada ` /home/"$USER"/log/"metrics_$log_time_format".log`
    - Agar hanya pemilik file yang bisa membaca dan mengubah file log maka gunakan 
        ```bash 
        chmod 600 -- /home/"$USER"/log/"metrics_$log_time_format".log
        ```

3. Membuat function `make_cron_jobs()`
    ```bash
    # Function untuk membuat cron jobs
    make_cron_jobs() {
        if [ ! -f "agregasi_cron" ]
        then
            # Copy cronjob system yang sudah ada dan taruh di file "agregasi_cron"
            crontab -l > agregasi_cron

            # Dilaksanakan setiap jam pada menit 59
            # Menggunakan 59 */1 * * * agar bisa membandingkan semua file "log per menit"
            # sebanyak 60 kali (menit ke 0 sampai menit ke 59)
            # Lokasi script didapatkan dengan absolute path menggunakan `pwd`
            # dan di awk untuk menghilangkan spasi pada nama direktori
            echo "59 */1 * * * "$(pwd | awk '{ gsub(/ /, "\\ ", $0); print; }')"/$0" >> agregasi_cron

            # masukkan isi dari agregasi_cron ke crontab system
            crontab agregasi_cron
        fi
        # Pastiin service buat ngejalanin cron udah nyala dan aktif
    }
    ```

    - Sebelumnya dicek terlebih dahulu apakah sudah ada file “agregasi_cron”, jika sudah ada tidak dibuat lagi
    - Copy cronjob yang sudah ada ke file “agregasi_cron”
    - File agregasi akan ditrigger untuk dijalankan setiap jam secara otomatis. Maka menggunakan  ` 59 */1 * * * ` , untuk dapat membandingkan semua file “log per menit” sebanyak 60 kali(menit ke 0 sampai menit ke 59)
    - Lokasi script didapatkan dengan absolute path menggunakan `pwd`
    - Dan di awk untuk menghilangkan spasi pada nama direktori dan simpan pada agregasi_cron
    - masukkan isi dari agregasi_cron ke crontab system
 
### Kendala Saat Pengerjaan

#### Di luar soal:

- Belajar cara menggunakan Git dan GitLab
- Belajar membuat markdown

#### **Soal 1**

- Tidak tahu bagaimana cara memasukkan variabel dari shell ke `awk`
- Tidak tahu bagaimana cara mengembalikan return value dari sebuah function
- Tidak tahu bagaimana cara meng-*assign* return value dari `awk` ke variabel shell
- Mencari cara untuk membandingkan variabel di shell dengan regular expression (regex) dan juga cara untuk menjadikan semua string menjadi lowercase
- Tidak tahu cara bagaimana membuat input user menjadi *hidden*
- Tidak tahu bagaimana memanipulasi string di bash:
    - Mendapatkan panjang string
    - Mengekstrak substring
    - Menjadikan string menjadi lowercase
- Sempat bingung saat memproses command `dl`, bagaimana cara mendapatkan nomor file terakhir (`PIC_XX`) tanpa harus menggunakan loop

#### **Soal 2**

- Saat demo, ternyata `mkdir` harus menggunakan `awk`. Jadi, semua `mkdir` di semua soal kami ganti menjadi menggunakan `awk`
- Tidak tahu bagaimana mengganti substring di `awk`, untuk menghilangkan double quotes (`"`) pada IP address
- Tidak tahu bagaimana syntax array di `awk`
- `curl` ternyata harus menggunakan opsi `-L` untuk menunggu redirect dari website agar tidak hanya terunduh file `html`-nya saja
- Sedikit ambigu untuk perhitungan rata-rata requests per jam

#### **Soal 3**

- Tidak tahu bagaimana syntax array multidimensional di `awk`
- Tidak terpikir bagaimana cara mem-*backup* cron jobs system yang sudah ada
- Kurang jelas untuk bagian perhitungan agregasi menit ke jam. Akhirnya, kami menyimpulkan dilakukan setiap jam pada menit ke 59 agar bisa dihitung dari 60 file (menit 0 sampai menit 59)

## Sumber
- [Google](https://www.google.com)
- [StackOverflow](https://stackoverflow.com)
- [GNU Awk User's Guide](https://www.gnu.org/software/gawk/manual/html_node/)
- [Unix & Linux Stack Exchange](https://unix.stackexchange.com/)
- [GitHub Modul 1 Sistem Operasi](https://github.com/arsitektur-jaringan-komputer/Modul-Sisop/tree/master/Modul1)
- [The Linux Documentation Project - Bash String Manipulation](https://tldp.org/LDP/abs/html/string-manipulation.html)
