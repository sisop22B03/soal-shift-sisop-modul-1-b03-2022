#!/bin/bash

# mengecek apakah sudah ada username yang sama
check_if_user_exists() {
    # karena awk tidak bisa mengembalikan return value,
    # maka output "print" dari awk disimpan ke
    # variabel user_exists
    user_exists=$(awk -v username="$username" '
        BEGIN {
            exists = 0
        }
        {
            if (username == $1) {
                exists = 1
            }
        }
        END {
            if (exists == 0)
                printf(0)
            else
                printf(1)
        }' ./users/user.txt)
}

# mengecek apakah password sesuai dengan kriteria
check_password_validity() {
    password_valid=1
    if [ ${#password} -lt 8 ] ||
        # =~ digunakan untuk membandingkan dengan regex
        [[ ! "$password" =~ [[:upper:]] ]] ||
        [[ ! "$password" =~ [[:lower:]] ]] ||
        [[ ! "$password" =~ ^[[:alnum:]]+$ ]] ||
        # ${string,,} -> ,, digunakan untuk menjadikan string ke lowercase
        [ ${password,,} == ${username,,} ]
    then
        password_valid=0
    fi
}

# registrasi username
register_username() {
    local flag=1
    while [ $flag -eq 1 ]
    do
        printf "Enter username: "
        read username

        check_if_user_exists
        if [ $user_exists == 1 ]
        then
            printf "REGISTER: ERROR User already exists\n"
            echo "$cDATE REGISTER: ERROR User already exists" >> log.txt
        else
            flag=0
        fi
    done  
}

# registrasi password
register_password() {
    local flag=1
    while [ $flag -eq 1 ]
    do
        printf "Enter password: "
        #-r agar backslash tidak meng-escape chars
        #-s agar text tampil hidden
        read -rs password

        check_password_validity
        if [ $password_valid -eq 0 ]
        then
            printf "Password tidak memenuhi kriteria!\n"
        else
            local flag=0
            echo "REGISTER: INFO User $username registered successfully"
            echo "$cDATE REGISTER: INFO User $username registered successfully" >> log.txt
        fi
    done
}

# main function
main() {
    # mkdir harus pake awk
    awk 'BEGIN {
        # buat bikin direktori
        cmd_mkdir="mkdir -p " file_name
        system(cmd_mkdir)
        close(cmd_mkdir)
    }'
    touch ./users/user.txt

    cDATE=$(date '+%m/%d/%y %H:%M:%S')

    register_username
    register_password
    echo "$username $password" >> ./users/user.txt
}

main
