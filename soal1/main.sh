#!/bin/bash

check_user_password() {
    password_check=$(awk -v username="$username" -v password="$password" '
                {
                    if (username == $1) {
                        if (password != $2)
                            printf(0)
                        else
                            printf(1)
                        exit
                    }
                }' ./users/user.txt)
}

download_gambar() {
    # ekstrak substring dari command "dl N"
    # format: ${string:position:length}
    local jumlah_gambar=${perintah:3}
    local file_name="$(date '+%Y-%m-%d')_$username"
    awk -v file_name=$file_name 'BEGIN {
        cmd_mkdir="mkdir -p " file_name
        system(cmd_mkdir)
        close(cmd_mkdir)
    }'
    local latest_number=0

    # Cek apakah udah ada file .zip dengan nama yang sama
    if [ -f "$file_name.zip" ]
    then
        # -n agar tidak overwrite file yang sudah ada
        unzip -n -P "$password" "$file_name.zip"

        # Cek nomor terakhir file PIC_XX
        # Argumen -t untuk mengurutkan output ls berdasarkan waktu file terbaru
        latest_number=$(ls -t ./"$file_name" | awk '
            BEGIN {
                FS="_"
            }
            {
                latest_number = $2
                print $2
                exit
            }')

        # Mengonversi
        # juga mengubah ke base-10, jika ada leading 0,
        # seperti PIC_0X
        latest_number=$((10#$latest_number))
    fi

    # mengunduh dan memberi nama file gambar
    for ((i=latest_number+1; i<=latest_number+jumlah_gambar; i=i+1))
    do
        if [ $i -lt 10 ]
        then
            # -L untuk menunggu redirect
            curl -L https://loremflickr.com/320/240 -o "./$file_name/PIC_0$i"
        else
            curl -L https://loremflickr.com/320/240 -o "./$file_name/PIC_$i"
        fi
    done

    # -r = recursive, biar bisa ngezip dari
    # dalem folder
    zip -u -P "$password" -r "$file_name.zip" "$file_name"
}

check_login_attempts() {
    awk -v username="$username" '
    BEGIN {
        gagal = 0
        berhasil = 0
    }
    {
        if ($3 == "LOGIN:") {
            if ($NF == username)
                ++gagal
            else if ($6 == username)
                ++berhasil
        }
    }
    END {
        print "Percobaan login berhasil:", berhasil
        print "Percobaan login gagal:", gagal
        print "Total percobaan login:", berhasil+gagal
    }' ./log.txt
}

user_login() {
    logged_in=0

    # diasumsikan username pasti benar
    # dan sudah terdaftar di ./users/user.txt
    # karena tidak ada penjelasan di soal
    printf "Enter username: "
    read username

    while [ $logged_in -eq 0 ]
    do
        printf "Enter password: "
        read -rs password

        check_user_password
        if [ $password_check -eq 0 ]
        then
            echo "Password salah!"
            echo "$cDATE LOGIN: ERROR Failed login attempt on user $username" >> log.txt
        else
            logged_in=1
            echo "LOGIN: INFO User $username logged in"
            echo "$cDATE LOGIN: INFO User $username logged in" >> log.txt
        fi
    done
}

main_program() {
    while [ $logged_in -eq 1 ]
    do
        echo "Masukkan perintah/command:"
        read perintah

        if [[ "$perintah" =~ ^dl ]]
        then
            download_gambar
        elif [[ "$perintah" == "att" ]]
        then
            check_login_attempts
        else
            printf "Perintah tidak ada!\n\
                Perintah yang tersedia:\n\
                1. dl\n\
                2. att\n"
        fi
    done
}

main() {
    cDATE=$(date '+%m/%d/%y %H:%M:%S')
    user_login
    main_program
}

main
